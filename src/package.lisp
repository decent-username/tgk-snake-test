(defpackage #:draw-utils
  (:use #:cl)
  (:nicknames "DRAWU")
  (:export #:draw-rect-from-center
           #:draw-tiles))

(defpackage #:snake-utils
  (:use #:cl
        #:draw-utils)
  (:nicknames "SNAKEU")
  (:export #:snake
           #:snake-part
           #:snake-body
           #:snake-body-length
           #:snake-dead-p
           #:snake-god-mode-p
           #:snake-speed
           #:snake-ready

           #:snake-head-direction
           #:snake-part-position
           #:snake-part-direction
           #:snake-part-previous

           #:snake-body-to-alist
           #:snake-spawn
           #:snake-ready-p
           #:snake-pos-to-alist
           #:snake-head-on-fruit-p
           #:valid-direction-p
           #:snake-hit-wall-p
           #:snake-hit-self-p

           #:snake-eat-fruit
           #:snake-lengthen!
           #:update!
           #:move-snake!
           #:speed-up!
           #:move-part!
           #:draw-snake))

(defpackage #:tgk-snake
  (:use #:cl)
  (:export #:play
           #:replay
           #:exit))
