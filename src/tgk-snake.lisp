(in-package #:tgk-snake)

;;;------------------------------------------------------------------------------
;;; Defining Global Variables/Constants
;;;------------------------------------------------------------------------------
(defparameter *initial-scale-factor* 2)
(defparameter *scale-factor* *initial-scale-factor*)

(defparameter *tile-size*    (* 16 *scale-factor*))
(defparameter *board-width*  16)
(defparameter *board-height* 9)
(defparameter *canvas-width*  (* *tile-size* *board-width*  *initial-scale-factor*))
(defparameter *canvas-height* (* *tile-size* *board-height* *initial-scale-factor*))


;;; Position Constants
(defparameter *origin*              (gamekit:vec2 0 0))
(defparameter *bottom-left-corner*  (gamekit:vec2 0 0))
(defparameter *top-left-corner*     (gamekit:vec2 0 *canvas-height*))
(defparameter *bottom-right-corner* (gamekit:vec2 *canvas-height* 0))
(defparameter *top-right-corner*    (gamekit:vec2 *canvas-width* *canvas-height*))
(defparameter *center*              (gamekit:vec2 (/ *canvas-width* 2)
                                                  (/ *canvas-height* 2)))

;;; Colors Constants
(defvar +black+ (gamekit:vec4 0 0 0 1))
(defvar +white+ (gamekit:vec4 1 1 1 1))
(defvar +red+   (gamekit:vec4 1 0 0 1))
(defvar +green+ (gamekit:vec4 0 1 0 1))
(defvar +blue+  (gamekit:vec4 0 0 1 1))

;;; Other Variables
(defparameter *board-dimensions* `(,*board-width* . ,*board-height*))

;; (defparameter *objects* (make-hash-table)) ; (setf (gethash 'key-name ht) value)
(defvar *fruits* '()) ;; association list
(defvar *walls*  '((0 . 1) (0 . 2) (0 . 3) (0 . 4)
                   (0 . 5) (0 . 6) (0 . 7))) ;; association list

(defparameter *tick* 0)

(defparameter *random-text-1* "random-text-1")
(defparameter *random-text-pos-1* (gamekit:vec2 *canvas-width*
                                                (* *canvas-height* 2)))
(defparameter *random-text-2* "random-text-2")
(defparameter *random-text-pos-2* (gamekit:vec2 *canvas-width*
                                                (* *canvas-height* 2)))

(defvar *win-text* "Dude, that's a big-ass snake.")
(defvar *win-text-pos* (gamekit:vec2 *canvas-width*
                                     (* *canvas-height* 2)))
(defparameter *game-over-text-1* "You've killed the snake...")
(defparameter *game-over-text-2* "good job.")

(defparameter *game-over-sound-player-p* nil)

(defparameter *player-snake* (snakeu:snake-spawn :board-dim *board-dimensions*
                                                 :pos '(6 . 3)
                                                 :body-length 4
                                                 :direction :down
                                                 :speed 60))
(defparameter *snake-color* (gamekit:vec4 0.1 0.5 0.35 1))
(defparameter *god-mode-color* (gamekit:vec4 0.8 0.7 0.2 1))
;;;------------------------------------------------------------------------------
;;; Define Game
;;;------------------------------------------------------------------------------
(gamekit:defgame snake () ()
                 (:viewport-width *canvas-width* )
                 (:viewport-height *canvas-height*)
                 (:viewport-title "snake")
                 (:prepare-resources t))

;;;------------------------------------------------------------------------------
;;; Defining Global Resources
;;;------------------------------------------------------------------------------
(gamekit:register-resource-package
 :keyword (asdf:system-relative-pathname :tgk-snake "assets/"))
;;; Sounds
(gamekit:define-sound :swish-30 "snd/misc/Swish30-Single.wav")
(gamekit:define-sound :magnum-pistol "snd/misc/357-Magnum-Pistol-Sho.wav")
(gamekit:define-sound :crash-metal "snd/misc/Crash-Metal-Shatter.wav")
(gamekit:define-sound :game-over "snd/misc/game-over.wav")
(gamekit:define-sound :god-mode "snd/misc/god-mode.ogg")
;;; Fonts
;; (gamekit:define-font :cleon-light  "font/Cleon-Light.ttf")
;; (gamekit:define-font :black-animal "font/Black-Animal.ttf")
(gamekit:define-font :fira-mono-bold "font/FiraMono-Bold.ttf")
(gamekit:define-font :fira-mono-medium "font/FiraMono-Medium.ttf")
(gamekit:define-font :fira-mono-regular "font/FiraMono-Regular.ttf")

;;; Sprites
(gamekit:define-image :background-1 "img/background-1.png")
(gamekit:define-image :background-2 "img/background-2.png")
(gamekit:define-image :grid         "img/grid.png")
;; (gamekit:define-image :0            "img/0.png")
;; (gamekit:define-image :1            "img/1.png")
;; (gamekit:define-image :2            "img/2.png")
;; (gamekit:define-image :3            "img/3.png")

;;;------------------------------------------------------------------------------
;;; Post Init
;;;------------------------------------------------------------------------------
(defmethod gamekit:post-initialize ((app snake))
  ;; (gamekit:bind-cursor (lambda (x y)
  ;;                        ))
  (gamekit:bind-button :up    :pressed
                       (lambda () (when (snakeu:valid-direction-p *player-snake* :up)
                                 (setf (snakeu:snake-part-direction
                                        (car (snakeu:snake-body *player-snake*))) :up)
                                 (format t "~&:up~%"))))
  (gamekit:bind-button :down  :pressed
                       (lambda () (when (snakeu:valid-direction-p *player-snake* :down)
                                 (setf (snakeu:snake-part-direction
                                        (car (snakeu:snake-body *player-snake*))) :down)
                                 (format t "~&:down~%"))))
  (gamekit:bind-button :left  :pressed
                       (lambda () (when (snakeu:valid-direction-p *player-snake* :left)
                                 (setf (snakeu:snake-part-direction
                                        (car (snakeu:snake-body *player-snake*))) :left)
                                 (format t "~&:left~%"))))
  (gamekit:bind-button :right :pressed
                       (lambda () (when (snakeu:valid-direction-p *player-snake* :right)
                                 (setf (snakeu:snake-part-direction
                                        (car (snakeu:snake-body *player-snake*))) :right)
                                 (format t "~&:right~%"))))
  )

;;;------------------------------------------------------------------------------
;;; Pre Destroy
;;;------------------------------------------------------------------------------
(defmethod gamekit:pre-destroy ((app snake))
  (trivial-garbage:gc :full t)
  (sb-ext:gc :full t)) ; this might not be portable to other Common Lisp implementations.


;;;------------------------------------------------------------------------------
;;; Utility Functions
;;;------------------------------------------------------------------------------
(defun real-time-seconds ()
  "Return seconds since certain point of time"
  (/ (get-internal-real-time) internal-time-units-per-second))

(defun update-position-jump (position time)
  "Update position vector depending on the time supplied
  HERE: jumping"
  (let* ((subsecond (nth-value 1 (truncate time)) ;; time without fraction
           )
         (angle (* 2 pi subsecond)))
    (setf (gamekit:y position) (+ 300 (* 100 (sin angle))))))


(defun update-position-circle (position time)
  "Update position vector depending on the time supplied
  HERE: circular movement"
  (let* ((subsecond (nth-value 1 (truncate time)))
         (angle (* 2 pi subsecond)))
    (setf (gamekit:x position) (+ (* (cos angle) 150) (/ *canvas-width* 6))
          (gamekit:y position) (+ (* (sin angle) 90)  (/ *canvas-height* 4)))))

(defun grey-shade (&key (value 0)  (opacity 1))
  "Value and opacity need to be between 0 and 1."
  (gamekit:vec4 value value value opacity))

(defun random-pos ()
  "generates a ranom position cons based on *board-width* and *board-height*"
  `(,(random *board-width*) . ,(random *board-height*)))

(defun valid-pos ()
  (let ((curr-pos (random-pos))
        (snake-body (snakeu:snake-body-to-alist *player-snake*)))
    (if (and (null (member curr-pos snake-body :test #'equal))
             (null (member curr-pos *walls* :test #'equal)))
        (return-from valid-pos curr-pos)
        (valid-pos))))
;;;------------------------------------------------------------------------------
;;; Every Frame Calculations
;;;------------------------------------------------------------------------------
(defmethod gamekit:act ((app snake))
  (update-position-circle *random-text-pos-1* (/ (real-time-seconds) 10))
  (update-position-circle *random-text-pos-2* (/ (real-time-seconds) 30))
  (update-position-jump   *win-text-pos*      (/ (real-time-seconds) 20))
  ;; (setf *tick* (+ *tick* (abs (sin 0.02))))
  ;; (setf *scale-factor* (* (abs (sin *tick*)) *initial-scale-factor*))
  ;; (setf (snakeu:snake-speed *player-snake*) (+ (snakeu:snake-speed *player-snake*)
  ;;                                              (+ *tick* (abs (sin 0.02)))))

  (unless (> (length *fruits*) 0)
      (push (valid-pos) *fruits*))

  (snakeu:update! *player-snake*) ;; update ready-p counter

  (when (snakeu:snake-ready-p *player-snake*)
    (snakeu:move-snake! *player-snake*))

  ;; kill snake when hitting a wall
  (when (and (snakeu:snake-hit-wall-p *player-snake* *walls*)
             (not (snakeu:snake-dead-p *player-snake*))
             (not (snakeu:snake-god-mode-p *player-snake*)))
    (gamekit:play-sound :crash-metal)
    (setf (snakeu:snake-dead-p *player-snake*) T)
    (setf *snake-color* (gamekit:vec4 0.4 0.1 0.05 1)))

  ;; kill snake when hitting itself
  (when (and (snakeu:snake-hit-self-p *player-snake*)
             (not (snakeu:snake-dead-p *player-snake*))
             (not (snakeu:snake-god-mode-p *player-snake*)))
    (gamekit:play-sound :crash-metal)
    (setf (snakeu:snake-dead-p *player-snake*) T)
    (setf *snake-color* (gamekit:vec4 0.4 0.1 0.05 1)))

  ;; eat fruit if on fruit
  (when (and (snakeu:snake-head-on-fruit-p *player-snake* *fruits*)
             (not (snakeu:snake-dead-p *player-snake*)))
    ;; remove fruits from *fruits*
    (gamekit:play-sound :magnum-pistol)
    (setf *fruits* (snakeu:snake-eat-fruit *player-snake* *fruits*))
    (snakeu:snake-lengthen! *player-snake*)
    (snakeu:speed-up! *player-snake*))

  ;; go into god mode when snake has eaten 20 fruits
  (when (and (> (snakeu:snake-body-length *player-snake*) 20)
             (not (snakeu:snake-god-mode-p *player-snake*)))
    (gamekit:play-sound :god-mode)
    (setf (snakeu:snake-god-mode-p *player-snake*) T)
    (setf *snake-color* *god-mode-color*))
  )

;;;------------------------------------------------------------------------------
;;; Every Frame Drawing
;;:------------------------------------------------------------------------------
(defmethod gamekit:draw ((app snake))
  (gamekit:with-pushed-canvas ()
    (gamekit:scale-canvas *scale-factor* *scale-factor*)
    ;; draw background
    (gamekit:draw-image *origin* :background-1)
    (gamekit:draw-image *origin* :grid)


    ;; draw boxes
    (drawu:draw-tiles *walls*
                      :tile-size   *tile-size*
                      :color (gamekit:vec4 0.2 0.4 0.6 1))

    (drawu:draw-tiles *fruits*
                      :tile-size *tile-size*
                      :color (gamekit:vec4 0.8 0.2 0.4 1))


    (snakeu:draw-snake *player-snake*
                       :tile-size *tile-size*
                       :color     *snake-color*)

    (when (snakeu:snake-dead-p *player-snake*)
       (when (null *game-over-sound-player-p*)
               (gamekit:play-sound :game-over)
               (setf *game-over-sound-player-p* T))

       (gamekit:draw-text *game-over-text-1*
                          (gamekit:vec2 20 200)
                          :fill-color (grey-shade :value 0.8)
                          :font (gamekit:make-font :fira-mono-regular 32))
       (gamekit:draw-text *game-over-text-2*
                          (gamekit:vec2 150 165)
                          :fill-color (grey-shade :value 0.8)
                          :font (gamekit:make-font :fira-mono-regular 32)))

    (when (snakeu:snake-god-mode-p *player-snake*)
        (gamekit:draw-text *win-text*
                         (gamekit:vec2 20 200)
                         :fill-color (grey-shade :value 1)
                         :font (gamekit:make-font :fira-mono-regular 32)))
    ))

;;;------------------------------------------------------------------------------
;;; Exported Functions
;;;------------------------------------------------------------------------------
(defun play ()
  (gamekit:start 'snake))

(defun replay ()
  (sb-ext:gc :full t)
  (setf *game-over-sound-player-p* nil)

  (setf *player-snake* (snakeu:snake-spawn :board-dim *board-dimensions*
                                           :pos '(6 . 3)
                                           :body-length 4
                                           :direction :down
                                           :speed 60))
  (setf *snake-color* (gamekit:vec4 0.1 0.5 0.35 1))
  (play))

(defun exit ()
  (gamekit:stop))
