(in-package #:snake-utils)

(defclass snake ()
  ((body
    :accessor snake-body ;; list of snake-parts
    :initarg  :body
    :initform '())
   (body-length
    :accessor snake-body-length
    :initarg :body-length
    :initform 0)
   (dead-p
    :accessor snake-dead-p
    :initform nil)
   (god-mode-p
    :accessor snake-god-mode-p
    :initform nil)
   (speed
    :accessor snake-speed
    :initarg  :speed
    :initform 60)
   (ready
    :accessor snake-ready
    :initform 0)
   ;; can later add other fields like power-up-status
   ))

(defclass snake-part ()
  ((position
    :accessor snake-part-position
    :initarg  :position)
   (direction
    :accessor snake-part-direction
    :initarg  :direction
    :initform :down)
   (previous
    :accessor snake-part-previous
    :initarg  :previous
    :initform nil)))

;;------------------------------------------------------------------------------
(defun snake-spawn (&key board-dim  (pos '(0 . 0)) (direction :down) (body-length 3) (speed 60))
  "Create instance of snake on board Returns `NIL' if snake doesn't fit on board.
ARGS:
`board-dim'   - a cons cell like '(16 . 9)
`pos'         - position of snake-head on board e.g.: '(3 5)
`direction'   - :up, :down, :left, :right
`body-length' - how many snake-parts the snake consists of"
  (let* ((x-dim (car board-dim))
         (y-dim (cdr board-dim))
         (x-pos (car pos))
         (y-pos (cdr pos)))
    (labels ((snake-make-body ()
               ;; create list out of `body-length' snake-parts.
               ;; Starting from the tail. Puts parts on `board'.
               (labels ((calc-x-pos (index)
                          (case direction
                            (:up    x-pos)
                            (:down  x-pos)
                            (:left  (+ x-pos (1- (- body-length index))))
                            (:right (- x-pos (1- (- body-length index))))))
                        (calc-y-pos (index)
                          (case direction
                            (:up    (- y-pos (1- (- body-length index))))
                            (:down  (+ y-pos (1- (- body-length index))))
                            (:left  y-pos)
                            (:right y-pos))))
                 (do* ((i 0 (1+ i))
                       (previous-part nil)
                       (curr-part nil)
                       (result '() (cons curr-part result)))
                      ((= i body-length) result)
                   (setf curr-part (make-instance 'snake-part
                                                  :position `(,(calc-x-pos i) . ,(calc-y-pos i))
                                                  :direction direction
                                                  :previous previous-part))
                   (setf previous-part curr-part))))

             (outside-board-p ()
               "Checks weather the snake fits completely onto the board."
               (labels ((head-not-on-board-p ()
                          (or (> x-pos x-dim)
                              (> y-pos y-dim)
                              (< x-pos 0)
                              (< y-pos 0)))
                        (tail-in-wall-p ()
                          (case direction
                            (:up    (> 0     (- y-pos (1- body-length))))
                            (:down  (< y-dim (+ y-pos (1- body-length))))
                            (:left  (< x-dim (+ x-pos (1- body-length))))
                            (:right (> 0     (- x-pos (1- body-length)))))))
                 (or (head-not-on-board-p)
                     (tail-in-wall-p)))))
      ;;----------------------------------------
      (unless (outside-board-p) ;; checks weather the snake can be created
        ;; Create snake object that has a reference to the board it's on.
        ;; Also modifies fields in array to reflect the snakes existence.
        (make-instance 'snake
                       :body   (snake-make-body)
                       :body-length body-length
                       :speed speed)))))


(defmethod snake-ready-p ((s snake))
  (< (snake-ready s) 0))

;;------------------------------------------------------------------------------
(defmethod update! ((s snake))
  (if (snake-ready-p s)
      (setf (snake-ready s) 1000)
      (decf (snake-ready s) (snake-speed s))))

;;------------------------------------------------------------------------------
(defun move-snake! (snake)
  (move-part! (car (snake-body snake))))

(defun move-part! (sp)
  ;; this method needs to to check for wrap around or collision
  ;; moves part and updates direction of snake-part-previous
  (let* ((curr-direction (snake-part-direction sp))
         (curr-position  (snake-part-position  sp))
         (board-width  16)
         (board-height 9))
    (case curr-direction
      (:up    (setf (cdr curr-position) (mod (1+ (cdr curr-position)) board-height)))
      (:down  (setf (cdr curr-position) (mod (1- (cdr curr-position)) board-height)))
      (:left  (setf (car curr-position) (mod (1- (car curr-position)) board-width)))
      (:right (setf (car curr-position) (mod (1+ (car curr-position)) board-width))))
    (if (null (snake-part-previous sp))
        (return-from move-part!)
        (progn
          (move-part! (snake-part-previous sp))
          (setf  (snake-part-direction (snake-part-previous sp)) curr-direction)))
    ))


;;------------------------------------------------------------------------------
(defmethod draw-snake ((s snake) &key tile-size (color (gamekit:vec4 0.5 0.5 0.5 1)))
  (let* ((body (snake-body s))
         (body-alist (snake-pos-to-alist body)))
    ;; (print body-alist)
    (drawu:draw-tiles body-alist
                      :tile-size tile-size
                      :color color)))

;;------------------------------------------------------------------------------
(defun snake-pos-to-alist (snake-body)
  ;; doesn't matter if it's reversed if it's just for drawing
  (labels ((snake-pos-to-alist-iter (snake-body result)
             (cond ((null snake-body) result)
                   (t (snake-pos-to-alist-iter
                       (cdr snake-body)
                       (cons (snake-part-position (car snake-body)) result))))))
    (snake-pos-to-alist-iter snake-body '())))

;;------------------------------------------------------------------------------
(defun snake-head-on-fruit-p (snake fruit-alist)
  ;; returns index
  (let* ((head (car (snake-body snake)))
         (head-pos (snake-part-position head)))
    (flet ((on-fruit-p (fruit-pos)
             (and (= (car head-pos) (car fruit-pos))
                  (= (cdr head-pos) (cdr fruit-pos)))))
      (dolist (fruit-pos fruit-alist nil) ; return nil if on no fruit
        (when (on-fruit-p fruit-pos)
          (return-from snake-head-on-fruit-p fruit-pos))))))

(defun snake-eat-fruit (snake fruits)
  ;; returns fruit to be eaten
  (dolist (fruit-pos fruits)
    (when (snake-head-on-fruit-p snake fruits)
      (return-from snake-eat-fruit (delete fruit-pos fruits :test #'equal)))))

;;------------------------------------------------------------------------------
(defun snake-lengthen! (snake)
  (flet ((calc-new-tail-pos (tail-direction tail-pos)
           (let ((tail-x-pos (car tail-pos))
                 (tail-y-pos (cdr tail-pos)))
             (case tail-direction
               (:up    `(,tail-x-pos      . ,(1- tail-y-pos)))
               (:down  `(,tail-x-pos      . ,(1+ tail-y-pos)))
               (:left  `(,(1+ tail-x-pos) . ,tail-y-pos))
               (:right `(,(1- tail-x-pos) . ,tail-y-pos))))))

    (let* ((body (snake-body snake))
           (tail (car (last body)))
           (tail-pos (snake-part-position tail))
           (tail-direction (snake-part-direction tail))
           (new-tail (make-instance 'snake-part
                                    :position  (calc-new-tail-pos tail-direction tail-pos)
                                    :direction tail-direction
                                    :previous  nil)))

      (nconc (snake-body snake) (list new-tail))
      (incf (snake-body-length snake))
      (setf (snake-part-previous tail) new-tail))))


;;------------------------------------------------------------------------------
(defun speed-up! (snake)
  (incf (snake-speed snake) 12))


;;------------------------------------------------------------------------------
(defun valid-direction-p (snake direction)
  (flet ((invalid-direction-p (direction1 direction2)
           (or (and (eq direction1 :up)    (eq direction2 :down))
               (and (eq direction1 :down)  (eq direction2 :up))
               (and (eq direction1 :left)  (eq direction2 :right))
               (and (eq direction1 :right) (eq direction2 :left)))))
      (let ((neck-direction (snake-part-direction (second (snake-body snake)))))
        (if (or (invalid-direction-p direction neck-direction)
                (invalid-direction-p neck-direction direction))
            nil
            T))))

;;------------------------------------------------------------------------------
(defun snake-body-to-alist (snake)
  (let ((snake-body (snake-body snake))
        (result '()))
    (dolist (part snake-body result)
      (let ((part-pos (snake-part-position part)))
        (setf result (cons part-pos result))))))

;;------------------------------------------------------------------------------
(defun snake-hit-wall-p (snake wall-alist)
  (let* ((head (car (snake-body snake)))
         (head-pos (snake-part-position head)))
    (flet ((on-wall-p (wall-pos)
             (and (= (car head-pos) (car wall-pos))
                  (= (cdr head-pos) (cdr wall-pos)))))
      (dolist (wall-pos wall-alist nil) ; return nil if on no fruit
        (when (on-wall-p wall-pos)
          (return-from snake-hit-wall-p wall-pos))))))

;;------------------------------------------------------------------------------
(defun snake-hit-self-p (snake)
  (let* ((body (snake-body snake))
         (head (car body))
         (head-pos (snake-part-position head)))
    (flet ((same-pos-p (part-pos)
             (and (= (car head-pos) (car part-pos))
                  (= (cdr head-pos) (cdr part-pos)))))
      (dolist (part (rest body) nil) ; loop through rest of body and check for collision
        (when (same-pos-p (snake-part-position part)) ;; snake-part-position is cons cell
          (return-from snake-hit-self-p part))))))
