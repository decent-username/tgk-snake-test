(in-package #:draw-utils)

(defun draw-rect-from-center (origin w h &key (fill-paint nil) (stroke-paint nil)
                                           (thickness 1.0) (rounding 0.0))
  (gamekit:draw-rect (gamekit:subt origin (gamekit:vec2 w h))
                     (* w 2) (* h 2)
                     :fill-paint   fill-paint
                     :stroke-paint stroke-paint
                     :thickness    thickness
                     :rounding     rounding))


(defun draw-tiles (points &key tile-size color)
  "`points' is an assoc list containing points to draw
   `color'"
  (labels ((calc-real-pos (pos)
             (+ (* pos tile-size) (/ tile-size 2)))
           (draw-tile (x y)
             (let ((x-pos (calc-real-pos x))
                   (y-pos (calc-real-pos y))
                   (thickness (/ tile-size 8))
                   (fill-paint color)
                   (stroke-paint (gamekit:mult color
                                               1.2)))
               (drawu:draw-rect-from-center (gamekit:vec2  x-pos y-pos)
                                            (- (/ tile-size 2) (/ thickness 2))
                                            (- (/ tile-size 2) (/ thickness 2))
                                            :fill-paint   fill-paint
                                            :stroke-paint stroke-paint
                                            :thickness    thickness
                                            :rounding     8))))
    ;;----------------------------------------------------------------------
    (dolist (elm points)
        (draw-tile (car elm) (cdr elm)))))
