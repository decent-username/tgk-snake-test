(asdf:defsystem :tgk-snake
  :description "This is just a simple tgk-snake game."
  :author "moldybits,
decent-username <decent-username@not-a-real-email.xyz>"
  :version "0.0.1"
  :depends-on (trivial-gamekit)
  :serial t
  :pathname "src/"
  :components
  ((:file "package")
   (:file "draw-utils")
   (:file "snake-utils")
   (:file "tgk-snake")))
